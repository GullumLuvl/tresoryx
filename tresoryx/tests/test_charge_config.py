#!/usr/bin/env python3

from tresoryx.communs import *
from io import StringIO


EXAMPLE_CONFIG=r"""
exercice:  # Pas encore pris en compte.
  feuille_validation: 'Classes'
  colonne_debut: 1
  colonne_fin: 11  # exclue
  ligne_debut: 4
  dtypes:
    # À utiliser également comme colonnes attendues
    'Date': date
    'Catégorie': category
    'Nom': str
    'Crédit': float
    'Débit': float
    'Solde': float
    'Mode': str
    'Classe': category
    'Intitulé': str
  precedents_soldes:
    ligne_debut: 2
    colonnes: [8, 9]
sortie_exercice:
  largeurs_colonnes:
    0: 15  #B
    1: 25  #C
    2: 20  #D
    8: 20  #J
    9: 40  #K

reference:
  Classe:
    charges:
    - &60 '60 - Achats'
    - &606 "606 - Achat matériel non stockable (EDF, GDF, Eau, produits d'entretien, petit équipement)"
    - &607 '607 - Achat de marchandises (livres, brochures, matériels…)'
    - &62 '62 - Autres Services Extérieurs'
    - &623 '623 - Publicité, publications relations publiques (annonces, expos…)'
    - &625 '625 - Déplacements, missions, réceptions'
    - &628 '628 - Divers'
    - &65 '65 - Autres charges de gestion courante'
    - &658 '658 - Cotisations liées à la vie statutaire'
    - &67 '67 - Charges Exceptionnelles'
    - &671 '671 - Charges except. sur op. de gestion (dons, créances irrécouvrables)'
    - &678 '678 - Autres charges sur manifestations except. (6/an)'
    produits:
    - &70 '70 - Vente de produits, services, marchandises'
    - &7063 '706.3 - Recettes buvettes'  # Point ne marche pas en clé.
    - &7064 '706.4 - Autres'
    - &707 '707 - Ventes matériels (livres, brochures, vente aux membres)'
    - &74 "74 - Subventions d'exploitation"
    #- 'Etat'
    - &741 '741 - Subvention municipale Ville de Montrouge'
    - &7421 '742.1 - Subvention départementale'
    - &744 '744 - Autres subventions'
    - &75 '75 - Autres produits de gestion courante'
    - &755 '755 - Remboursement de frais pour services rendus aux adhérents'
    - &756 '756 - Cotisations adhérents'

# Expressions régulières (= motifs) pour reconnaître la classe à partir de l'intitulé.
# Utilisez les guillemets *simples* (') pour les motifs !
regles_auto:
    charges:
        '\bAvances?\b':
              'Classe':    *625
              'Catégorie': 'Avance des frais de déplacement'
        '\baffiliation\b':
              'Classe':    *658
        '\blicences?\b|\bPrelevement du compte Cotisation\b':
              'Classe':    *658
              'Catégorie': 'Licences 2018-2019'
        '\bSubv(entions?)?\b':
             'Catégorie': 'Reversement subventions'
        '\b(Disque|Disc)s?\b':
              'Catégorie': 'Commande de disques'
        '\b(Maillot|Mayo|Uniforme|Tenue)s?\b':
              'Catégorie': 'Commande maillots'
        '\b(Disque|Disc|Maillot|Mayo|Uniforme|ecocup|casquette)s?\b':
              'Classe':    *607
        '\bAchats?\b':
              'Classe': *60
    produits:
        '\b(Rbsm?ts?|Remboursements?|Remb|Dettes?)':
              'Classe':    *755
              'Catégorie': 'Remboursements deplacements'
        '\b(Disque|Disc|Frisbee|Maillot|Mayo|tenue|uniforme)s?\b':
              'Classe':    *707
              'Catégorie': 'Vente maillots'
        '\binscr(\.|iption\b)|\badhesion\b|\bcotisation\b|\blicen[cs]e\b':
              'Classe':    *756
              'Catégorie': 'Cotisation 2018-2019'
        '\bbuvette\b':
              'Classe':    *7063
        '\bsubv(entions?)?\b':
              'Classe':    *741
              'Catégorie': 'Subventions mairie'
        '\bVentes?\b':
              'Classe':    *70
"""


def test_instanciate_regles_auto():
    config, regles_auto = parse_config(StringIO(EXAMPLE_CONFIG))
    assert isinstance(config, dict)
    assert isinstance(regles_auto, cree_regles_auto)

