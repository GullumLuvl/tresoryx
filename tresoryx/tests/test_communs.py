from tresoryx.communs import *
import pytest
from io import StringIO


class Test_european_to_us_number:
    def test_comma_to_dot(self):
        assert european_to_us_number('1,54') == '1.54'
    def test_remove_thousand_spacing(self):
        assert european_to_us_number('1 000') == '1000'


class Test_normalise_nom:

    empty_table = pd.DataFrame({'Relevé': [], 'Complet': []})
    entry_names = pd.Series(['Jacques Alfred', 'Mr. Henri Dès', 'OLGA YAGA',
                             'Double  Espace'])

    def test_output_type(self):
        assert isinstance(normalise_nom(self.entry_names, self.empty_table), pd.Series)

    def test_value0(self):
        assert normalise_nom(self.entry_names, self.empty_table).iloc[0] == 'Jacques Alfred'
    def test_value1(self):
        assert normalise_nom(self.entry_names, self.empty_table).iloc[1] == 'Henri Dès'
    def test_value2(self):
        assert normalise_nom(self.entry_names, self.empty_table).iloc[2] == 'Olga Yaga'
    def test_value3(self):
        assert normalise_nom(self.entry_names, self.empty_table).iloc[3] == 'Double Espace'


class Test_parse_nature:
    example_nature_str = """000001 VIR EUROPEEN
POUR: ASSOC Zzyzx
REF: 9856257828664
REMISE: Repas championnat
MOTIF: Repas championnat"""
    example_input = pd.Series(example_nature_str.split('\n'), index=['24/09/2018']*5)
    
    def test_output_type(self):
        assert isinstance(parse_nature(self.example_input), pd.DataFrame)

    def test_output_shape(self):
        assert parse_nature(self.example_input).shape == (7, 1)

    def test_output_index(self):
        assert set(parse_nature(self.example_input).index) == set(('DE', 'POUR', 'MOTIF', 'REF', 'REMISE', 'Info', 'Commentaire'))

class Test_fusionne_infosupp:
    pass

class Test_obtient_premiere_ligne:
    pass


@pytest.fixture()
def example_regles_auto():
    from test_charge_config import EXAMPLE_CONFIG
    yield parse_config(StringIO(EXAMPLE_CONFIG))[1]


class Test_devine_classe:

    def test_is_remboursement755(self, example_regles_auto):
        """Based on the EXAMPLE_CONFIG"""
        operation = pd.Series(['Rbst truc', np.NaN, 55.5],
                              index=['MOTIF', 'Débit', 'Crédit'])
        assert devine_classe(operation, example_regles_auto).startswith('755')
        

class Test_charge_releve_SoG:
    pass

class Test_operations_vers_exercice:
    def setUp(self):
        from .test_charge_config import EXAMPLE_CONFIG
        self.config, self.regles_auto = parse_config(StringIO(EXAMPLE_CONFIG))

